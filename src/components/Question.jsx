import { Grid } from "@material-ui/core";
import React, { useEffect, useRef, useState } from "react";
import { shadows } from "@material-ui/system";
import Box from "@material-ui/core/Box";
import { useSelector, useDispatch } from "react-redux";
import { styled } from "@material-ui/styles";
import { useLinkClickHandler } from "react-router-dom";

function Question({ data, setData }) {
  console.log(data);
  const [type, setType] = useState([]);
  const Ref = useRef(null);

  const [correct, setCorrect] = useState("0");
  const [subAnswer, setSubAnswer] = useState([]);
  const [timer, setTimer] = useState("");
  const [flagCorrect, setFlagCorrect] = useState(false);
  const [flagTimer, setFlagTimer] = useState(true);
  const [flagSubmit, setFlagSubmit] = useState(true);
  const [flagScore, setFlagScore] = useState(false);

  useEffect(() => {
    console.log(subAnswer);
    console.log(correct);
  }, [data]);

  useEffect(() => {
      if(timer === '00:00:00'){
        saveData()
      }
  }, [timer]);

  useEffect(() => {
    clearTimer(getDeadTime());
}, []);

 
  // Timer start

  const getTimeRemaining = (e) => {
    // debugger;
    const total = Date.parse(e) - Date.parse(new Date());
    const seconds = Math.floor((total / 1000) % 60);
    const minutes = Math.floor((total / 1000 / 60) % 60);
    const hours = Math.floor(((total / 1000) * 60 * 60) % 24);
    return {
      total,
      hours,
      minutes,
      seconds,
    };
  };
  const startTimer = (e) => {
    // debugger;
    let { total, hours, minutes, seconds } = getTimeRemaining(e);
    if (total >= 0) {
      // update the timer
      // check if less than 10 then we need to
      // add '0' at the begining of the variable
      setTimer(
        (hours > 9 ? hours : "0" + hours) +
          ":" +
          (minutes > 9 ? minutes : "0" + minutes) +
          ":" +
          (seconds > 9 ? seconds : "0" + seconds)
      );
    }

    // if(total===0 && hours === 0 && minutes === 0 && seconds ===0){
    //   saveData();
    // }
  };
  const clearTimer = (e) => {
    // debugger;
    // If you adjust it you should also need to
    // adjust the Endtime formula we are about
    // to code next
    setTimer("--:--:--");

    // If you try to remove this line the
    // updating of timer Variable will be
    // after 1000ms or 1sec
    if (Ref.current) clearInterval(Ref.current);
    const id = setInterval(() => {
      // debugger
      startTimer(e);
    }, 1000);
    Ref.current = id;
  };

  const getDeadTime = () => {
    // debugger;
    let deadline = new Date();

    // This is where you need to adjust if
    // you entend to add more time
    let length = data.length*30;
    deadline.setSeconds(deadline.getSeconds() + length);
    return deadline;
  };
  // Timer ends

  const handleEvent = (e) => {
    debugger;
    for (let ele of data) {
      console.log(ele);
      if (ele.questionId === e.target.id) {
        setSubAnswer({ ...subAnswer, [e.target.id]: e.target.value });
      }
    }
  };
  
  const saveData = () => {
    debugger;
    let count = 0;
    // clearTimer();
    let correct = (
      <i
        class="fa fa-check"
        style={{ fontSize: "48px", color: "green" }}
        aria-hidden="true"
      ></i>
    );
    let wrong = (
      <i
        class="fa fa-times"
        style={{ fontSize: "48px", color: "red" }}
        aria-hidden="true"
      ></i>
    );
    console.log(subAnswer);
    let copydata = JSON.parse(JSON.stringify(data));
    if(Object.keys(subAnswer).length > 0){
      for (let val in subAnswer) {
        for (let ele in copydata) {
          if (copydata[ele].questionId === val) {
            debugger;
            if (copydata[ele].correct === subAnswer[val]) {
              copydata[ele].verify = correct;
              count = count + 1;
              setData(copydata);
              setCorrect(count);
              
            } else {
              copydata[ele].verify = wrong;
              setData(copydata);
            }
          }
          if(copydata[ele].verify === "" ){
            setFlagCorrect(true)
          }
        }
        
      }

    }
    else{
      if(timer === '00:00:00'){
      setFlagCorrect(true)
      }
    }
    setFlagTimer(false);
    setFlagSubmit(false);
    setFlagScore(true);
  };
  return (
    <div>
      <h3
        style={{
          width: "60%",
          marginLeft: "20%",
          marginBottom: "1%",
          textAlign: "center",
        }}
      >
        {data[0].sub} Interview Questions
      </h3>
      {flagTimer && (
      <h2
        style={{
          width: "60%",
          marginLeft: "20%",
          marginBottom: "1%",
          textAlign: "center",
        }}
      >
        {timer}
      </h2>
      )}
      {data.map((val, index) => {
        return (
          <Grid
            container
            alignItems="center"
            style={{
              width: "60%",
              border: "1px solid gray",
              marginLeft: "20%",
              marginBottom: "1%",
              textAlign: "left",
              boxShadow: "5px 5px rgba(0, 0, 0, 0.2)",
            }}
          >
            <Grid item xs={10} style={{ height: "24vh" }}>
              <Grid item xs={12} style={{ height: "10vh", marginLeft: "2%" }}>
                {index + 1}- {val.question}
              </Grid>
              <Grid container>
                <Grid item xs={6} style={{ height: "7vh" }}>
                  <input
                    style={{ marginRight: "15px", marginLeft: "5%" }}
                    class="form-check-input"
                    type="radio"
                    name={val.questionId}
                    id={val.questionId}
                    checked={type.val}
                    value={val.answers[0]}
                    onClick={(e) => {
                      handleEvent(e);
                    }}
                  />
                  {flagSubmit === false ? (
                     <label class="form-check-label" for="flexRadioDefault1"
                     style={val.answers[0]=== val.correct? { color:'blue'} : {color : 'black'} }
                     >
                       {val.answers[0]}
                                          
                     </label>
                  ):(
                    <label class="form-check-label" for="flexRadioDefault1"
                                       >
                      {val.answers[0]}
                                         
                    </label>
                  )}
                 
                </Grid>
                <Grid item xs={6} style={{ height: "7vh" }}>
                  <input
                    style={{ marginRight: "15px" }}
                    class="form-check-input"
                    type="radio"
                    name={val.questionId}
                    id={val.questionId}
                    checked={type.val}
                    value={val.answers[1]}
                    onClick={(e) => {
                      handleEvent(e);
                    }}
                  />
                 {flagSubmit === false? (
                     <label class="form-check-label" for="flexRadioDefault1"
                     style={val.answers[1]=== val.correct? {color:'blue'} : {color : 'black'} }
                     >
                       {val.answers[1]}
                                          
                     </label>
                  ):(
                    <label class="form-check-label" for="flexRadioDefault1"
                                       >
                      {val.answers[1]}
                                         
                    </label>
                  )}
                 
                </Grid>
              </Grid>
              <Grid container>
                <Grid item xs={6} style={{ height: "7vh" }}>
                  <input
                    style={{ marginRight: "15px", marginLeft: "5%" }}
                    class="form-check-input"
                    type="radio"
                    name={val.questionId}
                    id={val.questionId}
                    checked={type.val}
                    value={val.answers[2]}
                    onClick={(e) => {
                      handleEvent(e);
                    }}
                  />
                  {flagSubmit === false? (
                     <label class="form-check-label" for="flexRadioDefault1"
                     style={val.answers[2]=== val.correct? { color:'blue'} : {color : 'black'} }
                     >
                       {val.answers[2]}
                                          
                     </label>
                  ):(
                    <label class="form-check-label" for="flexRadioDefault1"
                                       >
                      {val.answers[2]}
                                         
                    </label>
                  )}
                 
                </Grid>
                <Grid item xs={6} style={{ height: "7vh" }}>
                  <input
                    style={{ marginRight: "15px" }}
                    class="form-check-input"
                    type="radio"
                    name={val.questionId}
                    id={val.questionId}
                    checked={type.val}
                    value={val.answers[3]}
                    onClick={(e) => {
                      handleEvent(e);
                    }}
                  />
                   {flagSubmit === false ? (
                     <label class="form-check-label" for="flexRadioDefault1"
                     style={val.answers[3]=== val.correct ? {color:'blue'} : {color : 'black'} }
                     >
                       {val.answers[3]}
                                          
                     </label>
                  ):(
                    <label class="form-check-label" for="flexRadioDefault1"
                                       >
                      {val.answers[3]}
                                         
                    </label>
                  )}
                </Grid>
              </Grid>
            </Grid>
           
            <Grid
              item
              xs={2}
              style={{
                height: "24vh",
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                width: "100vw",
              }}
            >
              {val.verify}
              {console.log(val.verify,"********************************************")}
            </Grid>
            {/* {flagCorrect && (
            <Grid>
            <p style={{color:"red"}}> {val.correct}</p>
            </Grid>
            )} */}
          </Grid>
        );
      })}
      <Grid
        alignItems="center"
        style={{
          marginLeft: "50%",
        }}
      >
        {flagSubmit&& (
        <Grid>
          <button type="button" class="btn btn-primary" onClick={saveData}>
            Submit
          </button>
          </Grid>
        )}
        {flagScore&&(
          <Grid>
             <h6>
            Your Score{correct}/{data.length}
          </h6>
          </Grid>
          )}
      </Grid>
    </div>
  );
}

export default Question;
