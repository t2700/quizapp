import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { create } from "../redux/action";
import Question from "./Question";

function Landing({ data, setData }) {
  const [sub, setSub] = useState([]);
  const [selectedSub, setSelectedSub] = useState([]);

  const state = useSelector((state) => state);
  const dispatch = useDispatch();

  const navigate = useNavigate();

  useEffect(() => {
    console.log(state);
    let data = Object.keys(state.questions);
    console.log(data);
    data.push("All")
    setSub(data);
  }, state);
  const submitData = () => {
    debugger;
    let record;
    let allData = [];
    let storedData = [];
    console.log("submit");
    console.log(selectedSub, "seleceted sub");
    for (let ele in state.questions) {
       debugger;
      if (selectedSub.sub.val === ele) {
        // debugger;
        record = state.questions[ele];
        break;
      }
      else{
        
        // allData.push();
        console.log(state.questions[ele]);
        for(let e of state.questions[ele]){
          console.log(e);
          storedData.push(e)
          debugger
        }
        debugger
        record = storedData;
      }
    }
    setData(record);
    console.log(record);
    // dispatch(create(record));
    navigate("./question");
  };
  return (
    <div
      style={{
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        height: "100%",
        display: "flex",
        top: "40%",
      }}
    >
      <div style={{ border: "", marginTop: "10%" }}>
        <h3>Select Language</h3>
        <form style={{ padding: "8% 14% 13% 19%" }}>
          {sub.map((val) => {
           console.log(val);
            return (
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="radio"
                  name="flexRadioDefault"
                  id="flexRadioDefault1"
                  checked={selectedSub.val}
                  onClick={() => {
                    setSelectedSub({
                      ...selectedSub,
                      sub: { val },
                    });
                  }}
                />
                <label class="form-check-label" for="flexRadioDefault1">
                  {val}
                </label>
              </div>
            );
          })}
          <button type="button" class="btn btn-primary" onClick={submitData}>
            Next
          </button>
        </form>
      </div>
    </div>
  );
}

export default Landing;
