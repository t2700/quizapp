import * as types from './actionType';

export const create =(data)=>({
    type:types.CREATE,
    payload:data,
})
