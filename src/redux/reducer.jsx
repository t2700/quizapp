import { CREATE } from "./actionType";

let initialState = {
  questions: {
    HTML: [
      {
        question: "HTML stands for ?",
        answers: [
          "HighText Machine Language",
          "HyperText and links Markup Language",
          "HyperText Markup Language",
          "None of these",
        ],
        correct: "HyperText Markup Language",
        questionId: "099099",
        verify: "",
        sub:"HTML"
      },
      {
        question:
          "Which of the following element is responsible for making the text bold in HTML?",
        answers: ["<pre>", "<a>", "<b>", "<br>"],
        correct: "<b>",
        questionId: "099092",
        verify: "",
        sub:"HTML"
      },
      {
        question:
          "Which of the following tag is used to define options in a drop-down selection list?",
        answers: ["<select>", "<list>", "<dropdown>", "<option>"],
        correct: "<option>",
        questionId: "099080",
        verify: "",
        sub:"HTML"
      },
      {
        question:
          "Which of the following attribute is used to provide a unique name to an element?",
        answers: ["class", "id", "type", "None of the above"],
        correct: "id",
        questionId: "099081",
        verify: "",
        sub:"HTML"
      },
    ],
    CSS: [
      {
        question:
          "The property in CSS used to change the background color of an element is?",
        answers: ["bgcolor", "color", "background-color", "All of the above"],
        correct: "background-color",
        questionId: "099093",
        verify: "",
        sub:"CSS"
      },
      {
        question:
          "The CSS property used to control the element's font-size is?",
        answers: ["text-style", "text-size", "font-size", "None of the above"],
        correct: "font-size",
        questionId: "099094",
        verify: "",
        sub:"CSS"
      },
      {
        question:
          "Which of the following property is used as the shorthand property for the padding properties?",
        answers: ["padding-left", "padding-right", "padding", "All of the above"],
        correct: "padding",
        questionId: "099084",
        verify: "",
        sub:"CSS"
      },
      {
        question:
          "The CSS property used to specify the transparency of an element is ?",
        answers: ["opacity", "filter", "visibility", "overlay"],
        correct: "opacity",
        questionId: "099083",
        verify: "",
        sub:"CSS"
      },
    ],
    Javascript: [
      {
        question: "Which type of JavaScript language is?",
        answers: [
          "Object-Oriented",
          "Object-Based",
          "Assembly-language",
          "High-level",
        ],
        correct: "Object-Oriented",
        questionId: "099095",
        verify: "",
        sub:"JavaScript"
      },
      {
        question:
          "Which one of the following also known as Conditional Expression ?",
        answers: [
          "Alternative to if-else",
          "Switch statement",
          "If-then-else statement",
          "Immediate if",
        ],
        correct: "Immediate if",
        questionId: "099096",
        verify: "",
        sub:"JavaScript"

      },
      {
        question:
          " When interpreter encounters an empty statements, what it will do:?",
        answers: [
          "Shows a warning",
          "Prompts to complete the statement",
          "Throws an error",
          "Ignores the statements",
        ],
        correct: "Ignores the statements",
        questionId: "099085",
        verify: "",
        sub:"JavaScript"

      },
      {
        question:
          "The 'function' and 'var' are known as:?",
        answers: [
          "Keywords",
          "Data types",
          "Declaration statements",
          "Prototypes",
        ],
        correct: "Declaration statements",
        questionId: "099086",
        verify: "",
        sub:"JavaScript"

      },
    ],
    ReactJS: [
      {
        question: "Which of the following is the correct name of React.js?",
        answers: ["React", "React.js", "ReactJS", "All of the above"],
        correct: "All of the above",
        questionId: "099097",
        verify: "",
        sub:"ReactJS"

      },
      {
        question:
          "What of the following is used in React.js to increase performance?",
        answers: [
          "Original DOM",
          "Virtual DOM",
          "Both A and B.",
          "None of the above.",
        ],
        correct: "Virtual DOM",
        questionId: "099098",
        verify: "",
        sub:"ReactJS"

      },
      {
        question:
          "Which of the following acts as the input of a class-based component?",
        answers: [
          "Class",
          "Factory",
          "Render",
          "Props",
        ],
        correct: "Props",
        questionId: "099087",
        verify: "",
        sub:"ReactJS"

      },
      {
        question:
          "Which of the following keyword is used to create a class inheritance?",
        answers: [
          "Create",
          "Inherits",
          "Extends",
          "This",
        ],
        correct: "Extends",
        questionId: "099088",
        verify: "",
        sub:"ReactJS"

      },
    ],
  },
  answer: {},
};

const reducer = (state = initialState, action) => {
  debugger;
  switch (action.type) {
    case CREATE: {
      debugger;
      for (let ele in state.questions) {
        debugger;
        if (action.payload === ele) {
          debugger;
          state = state.questions[ele];
        }
      }
      return {
        state,
      };
    }
    default:
      return state;
  }
};
export default reducer;
