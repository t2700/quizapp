import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import Landing from './components/Landing';
import Question from './components/Question';
import React, { useState, useEffect } from "react";

function App() {
  const [data, setData] = useState([]);

  return (
    <div>
    <Router>
       <Routes>
         <Route exact path="/" element={<Landing  data={data} setData={setData}/>}/>
           <Route exact path="/question" element={<Question data={data} setData={setData}/>} />
        </Routes>
   </Router>

</div>
  );
}

export default App;
